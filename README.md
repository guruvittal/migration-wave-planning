# Migration Wave Planning

This repo is for architects who are involved in planning migration of enterprise workloads to Cloud.

## Getting started

Steps:

1. Install neo4j on a VM or container (Alternatively you can use Aura DB on Google Cloud which is a managed version of neo4j on GCP)

2. Create the following files from your Stratozone scans:
            - assets.csv (optional)
            - group.csv (optional)
            - dependency.csv (required)
            - Application.csv (optional)
            - AppServer.csv (required)
            - inventory.csv (required)

3. Run the cypher using the above files

4. Analyze your server network using the sample cyphers

5. Define the waves for the migration planning


## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
