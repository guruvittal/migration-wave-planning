//
// Loading Cyphers
//

// This is an optional cypher to add the inventory of VMs
LOAD CSV with headers FROM 'file:///assets.csv' AS line
CREATE (:Asset {name: line.assetName})

// This is an optional cypher will create all the solutions in play at the data center
LOAD CSV with headers FROM 'file:///group.csv' AS line CREATE (:solutionGroup {groupName: line.groupName})

// This is an optional cypher will attach the solution to its corresponding asset
LOAD CSV with headers FROM 'file:///asset_n_solutiongroup.csv' AS line
match (a:Asset{name: line.asset})
match (b:solutionGroup {groupName: line.solutionGroup})
merge (a)-[r:belongs_to]->(b)

// This cypher maps the dependencies between different VMs in the data center
:auto LOAD CSV with headers FROM 'file:///dependency.csv' AS line 
call {
with line
merge (a:Asset{name: line.initiatingDeviceName})
merge (i: IP {name:line.initiatingIpAddress})
merge (b:Asset{name: line.receivingDeviceName})
merge (j: IP {name:line.receivingIpAddress})
merge (service:Service {serviceName:coalesce(line.serviceName,”NOT”)})
merge (a)-[s:runs_service]->(service)
merge (a)-[:has_ip]->(i)
merge (b)-[:has_ip]->(j)
merge (a)-[r:depends_on]->(b)
set
r.initiatingPort = line.initiatingPort, 
r.initiatingIP = line.initiatingIpAddress, 
r.receivingPort = line.receivingPort, 
r.receivingIP = line.receivingIpAddress, 
r.protocol = line.protocol,
r.serviceName = line.serviceName
return count(*) as linesprocessed
}
in transactions of 1000 rows
return linesprocessed

// This is an optional cypher to add all the applications in the data center
LOAD CSV with headers FROM 'file:///Application.csv' AS line CREATE (:Application {appABBR: line.ABBR, appName:ApplicationName})

// This is an optional cypher enables mapping the apps to the VMs
:auto LOAD CSV with headers FROM 'file:///Apps_to_VMs.csv' AS line 
call {
with line
merge (a:Asset{name: line.assetName})
merge (i: IP {name:line.ipAddress})
merge (soft: Software {name: coalesce(line.installedSoftwareAppName,"NOT")})
merge (a)-[:has_primary_ip]->(i)
merge (soft)-[:installed_on]->(a)
set a.assetOsName = line.assetOsName
return count(*) as linesprocessed
}
in transactions of 1000 rows
return linesprocessed

//The cypher below loads the solution groups
:auto LOAD CSV with headers FROM 'file:///inventory.csv' AS line 
call {
with line
merge (a:Asset{name: line.name})
merge (i: IP {name:line.collectedIpAddress})
merge (sg: SolutionGroup {group: line.groupName})
merge (domain:Domain {name: coalesce(line.domain,”NOT”)})
merge (model: Model {name: line.model})
merge (a)-[:has_primary_ip]->(i)
merge (a)-[:belongs_to]->(sg)
merge (a)-[:has_domain]->(domain)
set
a.cloudFitScore = line.cloudFitScore,
a.roleName = line.roleName,
a.processorCount = line.processorCount,
a.totalProcessorCores = line.totalProcessorCores,
a.memoryInMb = line.memoryInMb,
a.driveTotalCapacityInGb = a.driveTotalCapacityInGb,
a.assetOsName = line.osName,
a.age = line.ageInMonths
return count(*) as linesprocessed
}
in transactions of 1000 rows
return linesprocessed

// The cypher below is optional and a custom cypher data for which may or may not be available
LOAD CSV with headers FROM 'file:///AppServer.csv' AS line 
match (a:Asset{name: line.asset})
Match (b:Application {appABBR: line.ABBR, appName: line.ApplicationName})
Merge (a)-[:has_app{percentUsed: line.PercentUsed}]-(b)

// Cypher to query the best candidates for early migration
MATCH (p:Asset)-[:depends_on]->(c:Asset), (p)-[:belongs_to*]-(q), (p)-[:has_app*]->(r)
WITH p, count(c) as rels, collect(c) as AssetList, q
WHERE rels < 4
RETURN p, AssetList, rels, q limit 30

//
// Analysis Cyphers
//

// Cypher to query the next best candidate for early migration and so on
MATCH (p:Asset)-[:depends_on]->(c:Asset), (p)-[:belongs_to]-(q), (p)-[:has_app*0..1]->(r)
WITH p, count(c) as rels, collect(c) as AssetList, q, r
WHERE rels < 6
RETURN p, AssetList, rels, q, r limit 30

// Cypher to identify the first grass root nodes
MATCH (p:Asset)-[r:depends_on*1..5]->(c:Asset)
WHERE NOT ()-[:depends_on]->(p) and NOT c.name = "Unscanned Device"
RETURN p, r, c limit 20

// This cypher identifies those VMs that are stand along VMs and hence early candidates for migration
MATCH (a)-[:depends_on]->(b)
WHERE NOT ()-[:depends_on]->(a)
RETURN a,b

// This cypher identifies those VMs that are stand alone VMs and hence early candidates for migration
MATCH (p:Asset)-[:depends_on]->(c:Asset)
WITH p, count(c) as rels, collect(c) as AssetList
WHERE rels < 3
RETURN p, AssetList, rels limit 100
